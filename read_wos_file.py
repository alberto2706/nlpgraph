import pdb
import nltk

filelocal = './data/sample_wos_data.txt'


def format_wos_data(wos_list):
    result = {}

    for doc_index, doc in enumerate(wos_list):
	result[doc_index] = {}
	if 'AB' in doc:
	    result[doc_index]['abstract'] = ' '.join(doc['AB'])
	else:
	    result[doc_index]['abstract'] = ''
	if 'TI' in doc:
	    result[doc_index]['title'] = ' '.join(doc['TI'])
	else:
	    result[doc_index]['title'] = ''
	if 'PY' in doc:
	    result[doc_index]['publication_year'] = ' '.join(doc['PY'])
	else:
	    result[doc_index]['publication_year'] = ''
	if 'AU' in doc:
	    result[doc_index]['authors'] = doc['AU']
	else:
	    result[doc_index]['authors'] = []
	if 'CR' in doc:
	    result[doc_index]['references'] = doc['CR']
	else:
	    result[doc_index]['references'] = []
	if 'ID' in doc:
	    result[doc_index]['keywords'] = (' '.join(doc['ID'])).split(';')
	else:
	    result[doc_index]['keywords'] = []
	if 'DI' in doc:
	    result[doc_index]['doi'] = doc['DI'][0]
	else:
	    result[doc_index]['doi'] = ''
    return result


def parse_lines(doc_filepath, chunk_size = 2048, sep = '\n'):
    string_buffer = ''
    with open(doc_filepath, 'r') as file_handle:
	for chunk in iter(lambda: file_handle.read(chunk_size), ''):
	    string_buffer += chunk
	    splitted_buffer_list = string_buffer.split(sep)
	    while len(splitted_buffer_list) > 1:
		line = splitted_buffer_list.pop(0)
		if line:
		    yield line
		else:
		    pass

	    string_buffer = ''.join(splitted_buffer_list)
	else:
	    yield string_buffer

def load_wos_data_ex(file_path, count = None, output = None):
    #TODO: cuidar de multiplos DOIs.
    result = list()
    doc_list = list()
    temp_field = str()
    temp_data = str()
    temp_doc = dict()
    for line in parse_lines(file_path):
	if line[:2] == 'ER':
	    pass
	    doc_list.append(temp_doc.copy())
	    temp_doc = {}
	    if count and count < len(doc_list):
		break
	elif line[:2] == 'EF':
	    pass
	elif line[:2] != '  ':
	    pass
	    temp_field = line[:2]
	    temp_doc[temp_field] = []
	    temp_data = line[3:]
	    temp_doc[temp_field].append(temp_data)
	elif line[:2] == '  ':
	    pass
	    temp_data = line[3:]
	    temp_doc[temp_field].append(temp_data)
	else:
	    pass

    result = format_wos_data(doc_list)

    return result

wos_dicio = load_wos_data_ex(filelocal)

texto = ''

for key, value in wos_dicio.items():

	texto = texto + ' ' + value['abstract']

words = nltk.word_tokenize(texto)
print 'quantidade palavras: ', len(words)

# Remove single-character tokens
words = [word for word in words if len(word) > 4]
print 'quantidade palavras apos removao de palavras com tamanho menor que 4: ',len(words)

#pdb.set_trace()
freq = nltk.FreqDist(words)
#pdb.set_trace()

print 'apresentando grafico com 20 maiores frequencias'
#TODO: rotacionar o eixo x das palavras para melhor visualiacao 
freq.plot(20, cumulative=False)