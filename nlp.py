import warnings
import math
import cPickle as pickle
from collections import defaultdict
from pprint import pprint
from time import clock

import matplotlib.pyplot as plt
import pandas as pd

from gensim.corpora import Dictionary, MmCorpus
from gensim.models.ldamulticore import LdaMulticore
from gensim.models import Phrases
from gensim.models.phrases import Phraser

import nltk
import spacy
import en_core_web_sm

#import pyLDAvis
#import pyLDAvis.gensim

#from spacy.en import English
#nlp = spacy.load('en')
#nlp = English()

nlp = en_core_web_sm.load()
nltk_exclusion_list = nltk.corpus.stopwords.words('english')
nltk_exclusion_list += [u'right', u'all', u'reserve']
#spacy_exclusion_list = en_core_web_sm.STOP_WORDS

global_clock = defaultdict(list)

def sample_machado(filter=''):
    valid_filters = set(['teatro', 'poesia', 'romance', 'cronica', 'traducao', 
			 'miscelanea', 'contos', 'critica'])                                            
    if not filter in valid_filters:
	filter = ''
    machado = nltk.corpus.machado
    machado_files = machado.fileids()
    machado_stories = [machado.open(m).read() for m in machado_files if filter in m]
    result = dict(zip(machado_files, machado_stories))
    #conto = contos_machado[0]
    #conto_tokens = nltk.word_tokenize(conto)
    #conto_text = nltk.Text(conto_tokens)
    return result

 #TODO: ???
class NgramAnalysis(object):
    def __init__():
	self.label = ''
	self.n = 0
	self.models = []
	self.models_file_handle = []
	#... dictionary, ngram, etc

def load_wos_data(file_path, count = None, output = None):
    result = list()
    doc_list = list()

    input_file = open(file_path, 'r')
    file_string = input_file.read()
    input_file.close()
    lines = file_string.split('\n')

    temp_field = str()
    temp_data = str()
    temp_doc = dict()

    for line in lines:
	if line[:2] == 'ER':
	    pass
	    doc_list.append(temp_doc.copy())
	    temp_doc = {}
	    if count and count < len(doc_list):
		break

	elif line[:2] == 'EF':
	    pass

	elif line[:2] != '  ':
	    pass
	    temp_field = line[:2]
	    temp_doc[temp_field] = []
	    temp_data = line[3:]
	    temp_doc[temp_field].append(temp_data)

	elif line[:2] == '  ':
	    pass
	    temp_data = line[3:]
	    temp_doc[temp_field].append(temp_data)

	else:
	    pass

    result = format_wos_data(doc_list)
	    
    return result

def format_wos_data(wos_list):
    result = {}

    for doc_index, doc in enumerate(wos_list):
	result[doc_index] = {}
	if 'AB' in doc:
	    result[doc_index]['abstract'] = ' '.join(doc['AB'])
	else:
	    result[doc_index]['abstract'] = ''
	if 'TI' in doc:
	    result[doc_index]['title'] = ' '.join(doc['TI'])
	else:
	    result[doc_index]['title'] = ''
	if 'PY' in doc:
	    result[doc_index]['publication_year'] = ' '.join(doc['PY'])
	else:
	    result[doc_index]['publication_year'] = ''
	if 'AU' in doc:
	    result[doc_index]['authors'] = doc['AU']
	else:
	    result[doc_index]['authors'] = []
	if 'CR' in doc:
	    result[doc_index]['references'] = doc['CR']
	else:
	    result[doc_index]['references'] = []
	if 'ID' in doc:
	    result[doc_index]['keywords'] = (' '.join(doc['ID'])).split(';')
	else:
	    result[doc_index]['keywords'] = []
	if 'DI' in doc:
	    result[doc_index]['doi'] = doc['DI'][0]
	else:
	    result[doc_index]['doi'] = ''

    return result

def make_doi_table(doc_dict):
    result = dict()
    for doc in doc_dict:
	result[doc_dict[doc]['doi']] = doc
    return result

def doi_from_node(node): 
    result = node.split('DOI ')
    if len(result) > 1:
	return result[1]
    else:
	return ''

def nltk_process_abstract(doc):
    abstract_tokens = nltk.word_tokenize(doc['abstract'])
    abstract_text = nltk.Text(abstract_tokens)
    abstract_tags = nltk.pos_tag(abstract_text)

    tags_fd = nltk.FreqDist(tag for (word, tag) in abstract_tags)
    return tags_fd


def plot_token_histogram(token_freq_dict):
    plt.bar(range(len(token_freq_dict.values())), token_freq_dict.values(),align='center')
    plt.xticks(range(len(token_freq_dict.keys())),token_freq_dict.keys())

def abstract_nlp(clusters, exclusion_list = nltk_exclusion_list):
    nltk_analysis_result = dict()
    for p in clusters:
	p_abstract = ''
	for node in clusters[p]:
	    try:
		node_doi = doi_from_node(node)
		if node_doi:
		    p_abstract += doc_dict[doi_table[node_doi]]['abstract']
		    p_abstract += '\n'
		else:
		    pass
	    except:
		print 'EXCEPT(abstract_nlp) -- node:', node
		pass

	abstract_tokens = nltk.word_tokenize(p_abstract)
	abstract_text = nltk.Text(abstract_tokens)
	abstract_fd = nltk.FreqDist(abstract_text)
	nltk_analysis_result[p] = list()

	for f in abstract_fd:
	    if not f in exclusion_list:
		nltk_analysis_result[p].append(f)

    return nltk_analysis_result

def cluster_analysis(network, doc_dict, doi_table, ngram_model, narrative = None, build_folder = None):
    clusters = network['clusters']

    bigram_model = ngram_model[0]
    trigram_model = ngram_model[1]

    nlp_clusters = defaultdict(list)

    for c in clusters:
	print c
	for n in clusters[c]:
	    print n
	    doi = doi_from_node(n)

	    if not doi:
		continue

	    if not doi in doi_table:
		continue

	    doc_key = doi_table[doi]
	    nlp_clusters[c].append(doc_key)

    token_freq = dict()

    for c in nlp_clusters:
	token_freq[c] = defaultdict(int)
	for doc in nlp_clusters[c]:

	    doc_abstract = doc_dict[doc]['abstract']

	    parsed_doc_abstract = nlp(unicode(doc_abstract))

	    abstract_unigrams = [token.lemma_ for token in parsed_doc_abstract
						if not token.is_punct and
						not token.is_stop]

	    abstract_bigrams = bigram_model[abstract_unigrams]
	    abstract_trigrams = trigram_model[abstract_bigrams]

	    for abstract_token in abstract_trigrams:
		token_freq[c][abstract_token] += 1

	plt.figure('cluster {}'.format(c))
	plot_token_histogram(token_freq[c])
	if build_folder:
	    plt.savefig('{}/cluster{}.png'.format(build_folder,c))

    for c in token_freq:
	print('\ncluster {}:\ntokens, freq:'.format(c))
	for token in token_freq[c]:
	    if token_freq[c][token] > 1:
		print('{}, {}'.format(token, token_freq[c][token]))
    

    print 'waldo'
    narrative['nlp_clusters'] = nlp_clusters.copy()
    narrative['token_freq'] = token_freq.copy()

    print 'func(cluster_analysis):', narrative

def save_obj(obj, filename):
    file_handle = open(filename, 'w')
    pickle.dump(obj, file_handle)
    file_handle.close()

#########################################################
###NOTE: config #########################################
#########################################################

data_folder = './data'
project_folder = './projects'
data_filename = '/sample_wos_data.txt'
project_name = '/untitled'

nlp_doc_count = 800

#########################################################
#########################################################
#########################################################

doc_dict = load_wos_data(data_folder+data_filename, count=nlp_doc_count)
doi_table = make_doi_table(doc_dict)

def spacy_ngram_model(doc_dict, field = 'abstract', n = 0):
    string_corpus = u'.\n'.join([doc_dict[doc][field] for doc in doc_dict])
    parsed_corpus = nlp(string_corpus)

    sentences = [[token.lemma_ for token in sent
		    if not token.is_punct and
		    not token.is_space and
		    not token.is_stop]
		     for sent in parsed_corpus.sents]

    phrases = Phrases(sentences)
    bigram_model = Phraser(phrases)
    trigram_model = Phrases(bigram_model[sentences])
    return (phrases, bigram_model, trigram_model)

is_stop = lambda t : t.is_punct or t.is_space or t.is_stop

def gen_corpus(doc_dict, field = 'abstract'): 
    for doc in doc_dict: 
	yield unicode(doc_dict[doc][field])

def gen_lemmatized_sents(doc_dict):
    for parsed_corpus in nlp.pipe(gen_corpus(doc_dict), batch_size=400, n_threads=4):
	for sent in parsed_corpus.sents:
	    yield [token.lemma_ for token in sent if not token.lemma_ in nltk_exclusion_list]
	    #yield u' '.join([token.lemma_ for token in sent if not is_punct(token)])

def ngram_models(doc_dict, field='abstract'):
    sentences = [sent for sent in gen_lemmatized_sents(doc_dict)]
    unigram_model = Phrases(sentences)
    bigram_model = Phraser(unigram_model)
    trigram_model = Phrases(bigram_model[sentences])
    return (unigram_model, bigram_model, trigram_model)

def make_ngram_dictionary(parsed_text, ngrams, sent_gen = None):
    bigram_model = ngrams[1]
    trigram_model = ngrams[2]

    #parsed_text = nlp(unicode(input_text))

    if sent_gen:
	sentences = [sent for sent in gen_lemmatized_sents(doc_dict)]
    else:
	sentences = [[token.lemma_ for token in sent
			    if not token.is_punct and
			    not token.is_space and
			    not token.is_stop]
			     for sent in parsed_text.sents]

    trigram_dictionary = Dictionary([t for t in ngrams[2][ngrams[1][sentences]]])

    return trigram_dictionary

def text_topics(parsed_text, ngrams, ngram_dictionary, topic_number = 1, dict_filter = None,
		sent_gen = None):
    if dict_filter:
	no_below = dict_filter[0]
	no_above = dict_filter[1]
	ngram_dictionary.filter_extremes(no_below=no_below, no_above=no_above)
	ngram_dictionary.compactify()

    #parsed_text = nlp(unicode(input_text))

    if sent_gen:
	sentences = [sent for sent in gen_lemmatized_sents(doc_dict)]
	ngram_d2b = ngram_dictionary.doc2bow([t for t in sent for sent in sentences])
    else:
	ngram_d2b = ngram_dictionary.doc2bow([unicode(t.lemma_) for t in parsed_text])

    #ngram_file_handle = open('ngram.temp', 'w')
    MmCorpus.serialize('ngram.temp', [ngram_d2b])
    ngram_corpus = MmCorpus('ngram.temp')

    topic_lda = LdaMulticore(ngram_corpus,
			      num_topics = topic_number,
			      id2word = ngram_dictionary,
			      workers=2)

    if True:
	for t in range(topic_number):
	    print '\ntopic_num = {}'.format(t)
	    for term, freq in topic_lda.show_topic(t, topn=20):
		print term, freq

    return topic_lda

def apply_lda(parsed_text, ngrams, ngram_dictionary, lda_model, sent_gen = None):
    if sent_gen:
	unigram_ab = [token for token in sent_gen
			if not token in nltk_exclusion_list]
    else:	
	unigram_ab = [token.lemma_ for token in parsed_text
			if not is_stop(token)]

    bigram_model = ngrams[1]
    bigram_ab = bigram_model[unigram_ab]
    trigram_model = ngrams[2]
    trigram_ab = trigram_model[bigram_ab]

    trigram_ab = [t for t in trigram_ab if not t in nltk_exclusion_list]
    input_text_bow = ngram_dictionary.doc2bow([unicode(t) for t in [s for s in trigram_ab]])

    input_text_lda = lda_model[input_text_bow]

    if False:
	print('f:apply_lda:')
	print('topic_n, freq:')
	for topic_n, freq in input_text_lda:
	    print topic_n, freq
	print('')

    return input_text_lda


global_clock['ngram_models'].append(clock())
ngrams = ngram_models(doc_dict)
gen_sents = gen_lemmatized_sents(doc_dict)
#ngrams = spacy_ngram_model(doc_dict)
global_clock['ngram_models'].append(clock())

global_clock['sample_gathering'].append(clock())
sample_abstracts = [doc_dict[doc]['abstract'] for doc in doc_dict]
input_text = u'\n'.join(sample_abstracts)
parsed_text = None#nlp(unicode(input_text))
global_clock['sample_gathering'].append(clock())

global_clock['ngram_dict'].append(clock())
ngram_dict = make_ngram_dictionary(parsed_text, ngrams, gen_sents)
global_clock['ngram_dict'].append(clock())

global_clock['lda_model'].append(clock())
lda_model = text_topics(parsed_text, ngrams, ngram_dict, topic_number = 1, dict_filter=(3,1.0), sent_gen = gen_sents)
global_clock['lda_model'].append(clock())

global_clock['lda_appied'].append(clock())
#apply_lda(parsed_text, ngrams, ngram_dict, lda_model, gen_sents)
global_clock['lda_appied'].append(clock())

print('nlp.py profiling:')
for c in global_clock:
    print('clock: {} -> dt = {}'.format(c, global_clock[c][1] - global_clock[c][0]))
print('-------------')
    
sa = unicode(sample_abstracts[0])
sa = u'\n'.join(sample_abstracts[:20])

apply_lda(nlp(sa), ngrams, ngram_dict, lda_model)



###NOTE: roadmap (com spacy e gensim)
###import spacy
###
###nlp = space.load('en')
###
###parsed_abstract = nlp(abstract_text)
###
###unigram_ab = [token.lemma_ for token in parsed_abstract if not token.is_punct]
###
###bigram_ab = bigram_model[unigram_ab]
###
###trigram_ab = trigram_model[bigram_ab]
###
###trigram_ab = [term for term in trigram_ab if not term in spacy.en.STOPWORDS]
###
###abstract_bow = trigram_dictionary.doc2bow(trigram_ab)
###
###abstract_lda = lda[abstract_bow]

#parsed_text = nlp(unicode(raw_text))
#sentences = parsed_text.sents #ou spans = parsed...
#entities = parsed_text.ents
#original_list = [token.orth_ for token in parsed_text]
#lowercased_list =[token.lower_ for token in parsed_text]
#lemma_list =[token.lemma_ for token in parsed_text]
#shape_list =[token.shape_ for token in parsed_text]
#prefix_list =[token.prefix_ for token in parsed_text]
#suffix_list =[token.suffix_ for token in parsed_text]
#log_probability_list =[token.prob for token in parsed_text]
#Brown_cluster_list =[token.cluster for token in parsed_text]
#token_ent_type = [token.ent_type_ for token in parsed_text]
#token_iob = [token.ent_iob_ for token in parsed_text]

