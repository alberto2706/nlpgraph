RES = resources
TARGET = main
PY = python

compile:
	clear;
	$(PY) $(TARGET).py;

profile:
	$(PY) -m cProfile -o $(RES)/$(TARGET).prof $(TARGET).py;
	snakeviz $(RES)/$(TARGET).prof;
