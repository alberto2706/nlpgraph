from time import clock
from collections import defaultdict
import os, errno, sys
import cPickle as pickle
import threading

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import networkx as nx
import community as cm

import spacy
import en_core_web_sm
import nltk
from gensim.models import Phrases
from gensim.models.phrases import Phraser
#nlp = spacy.load('en')
nlp = en_core_web_sm.load()

debug_set = set(['nlp', 'setup', 'nw'])

partition_colors = ['#DD0000', '#00DD00', '#0000DD', '#DDDD00',
	            '#00DDDD', '#DD00DD', '#880000', '#008800',
          	    '#000088', '#880088', '#888800', '#008888',
	            '#440000', '#004400', '#000044', '#440044',
          	    '#444400', '#004444', '#220000', '#002200',
          	    '#000022', '#220022', '#222200', '#002222']

global_narrative = dict()
global_word_exclusion_list = list()
if nltk:
    global_word_exclusion_list = nltk.corpus.stopwords.words('english')

class Debug(object):
    level = 0
    func_clock = defaultdict(list)
    def __init__(self,  group, inline_logging = False):
	self.group = set(group) if type(group) == list else set([group])
	self.inside_debug_set = False
	if self.group & debug_set:
	    self.inside_debug_set = True
	self.inline_logging = inline_logging
	self.func_calls = 0

    def __call__(self, f):
	if not self.inside_debug_set:
	    return f
	def new_f(*args, **kwargs):
	    self.func_calls += 1
	    tab_level = '  '*Debug.level
	    Debug.level += 1
	    cond_ellipses = lambda message, max_size: str(message) if len(str(message)) < max_size else str(message)[:max_size] + '...'
	    self.debug_print('{}func: {} (group: {})'.format(tab_level, f.__name__, self.group))
	    self.debug_print('{t}call: {message}'.format(t=tab_level, message = self.func_calls))
	    self.debug_print('{t}args: {message}'.format(t=tab_level, message = cond_ellipses(args, 70)))
	    self.debug_print('{t}kwargs: {message:.70}'.format(t=tab_level, message = str(kwargs) + '...' if len(str(kwargs)) > 70 else str(kwargs)))
	    Debug.func_clock[f.__name__].append(clock())
	    f_result = f(*args, **kwargs)
	    Debug.func_clock[f.__name__].append(clock())
	    self.debug_print('{t}delta: {message}'.format(t=tab_level, message = self.time_delta(f.__name__)))
	    self.debug_print('{t}result: {message}'.format(t=tab_level, message = cond_ellipses(f_result, 70)))
	    Debug.level -= 1
	    if Debug.level == 0:
		print('\n')
	    return f_result
	return new_f
    
    def debug_print(self, message):
	debug_end_string = ' ' if self.inline_logging else '\n'
	if self.inside_debug_set:
	    print(message),
	    print(debug_end_string),
	else:
	    pass

    def time_delta(self, name):
	return Debug.func_clock[name][1] - Debug.func_clock[name][0]

class Network(object):
    def __init__(self):
	self.nodes = defaultdict(int)
	self.edges = defaultdict(int)
	self.doi_table = dict()
	self.graph = None
	self.graphGC = None
	self.nodes_bc = None
	self.nodes_pos = None
	self.partitions = None
	self.clusters = None
	self.df = None

    @Debug('nw')
    def giant_component(self, inplace = False):
	self.graphGC = max(nx.connected_component_subgraphs(self.graph), key=len)
	if inplace:
	    self.graph = self.graphGC

    def giant_component_th(self, inplace = False):
	t = threading.Thread(target = self.giant_component_)
	t.start()
	t.join()

    def eval_clusters(self):
	self.clusters = {c:[node for node in self.graph.nodes() if self.partitions[node] == c] for c in self.partitions.values()}

    def __repr__(self):
	return 'nw:{} nodes, {} edges'.format(len(self.nodes), len(self.edges))

    @Debug('nw')
    def eval_betweenness_centrality(self, normalized = True, pos = 0, output = None):
	self.nodes_bc = nx.betweenness_centrality(self.graph, normalized = True)
	if output:
	    output.put((pos, self.nodes_bc))

    @Debug('nw')
    def eval_pos(self, pos = 1, output = None):
	self.nodes_pos = nx.spring_layout(self.graph)
	if output:
	    output.put((pos, self.positions))

    @Debug('nw')
    def eval_partitions(self, pos = 2, output = None):
	self.partitions = cm.best_partition(self.graph)
	if output:
	    output.put((pos, self.partitions))
	
    def analysis(self):
	thread_list = list()
	thread_list.append(threading.Thread(target = self.eval_betweenness_centrality))
	thread_list.append(threading.Thread(target = self.eval_pos))
	thread_list.append(threading.Thread(target = self.eval_partitions))
	for thread in thread_list: thread.start()
	for thread in thread_list: thread.join()

    def make_df(self, doc_dict):
	doc_columns = ['doc_key',
		       'authors',
		       'doi',
		       'abstract',
		       'title',
		       'publication_year',
		       'references',
		       'keywords']

	doc_data = [[doc_key,
		     doc_dict[doc_key]['authors'],
		     doc_dict[doc_key]['doi'],
		     doc_dict[doc_key]['abstract'],
		     doc_dict[doc_key]['title'],
		     doc_dict[doc_key]['publication_year'],
		     doc_dict[doc_key]['references'],
		     doc_dict[doc_key]['keywords']] for doc_key in doc_dict]

	self.df = pd.DataFrame(data = doc_data, columns = doc_columns).set_index('doc_key')

	node_doi_table = {n:d for d,n in self.doi_table.items()}
	node_doi_df = pd.DataFrame(data = [[node, node_doi_table[node]] 
				   for node in node_doi_table], 
				   columns = ['node', 'doi'])

	cluster_df = pd.DataFrame(data = [[n,p] for n,p in self.partitions.items()], 
				  columns = ['node', 'cluster'])

	self.df = pd.merge(left = self.df, right = node_doi_df, left_on = 'doi', right_on = 'doi', how='outer')
	self.df = pd.merge(left = self.df, right = cluster_df, left_on = 'node', right_on = 'node', how='outer')

    def missing_data_rate(self, col1, col2):
	#NOTE: Essa funcao conta a taxa de pares incompletos (col1, col2) no DataFrame (apos o
	#merge). Para sample_wos_data.txt e o par (node, doi), esse valor eh 0.94, i.e., apenas ~5% das
	#referencias citadas pelos artigos colhidos no WoS estao entre estes.
	#Essa taxa pode ser um pouco menor quando forem considerados os nos que possuem multiplos DOIs.
	result = (sum([0 if len(set([None, '', np.NaN]) & set(pair)) else 1
		  for pair in zip(doc_df[col1], doc_df[col2])]))/float(doc_df.index.size)
	return result

    def make_labels(self, threshold = 0, style='small'):
	labels = dict()
	if type(threshold) == int:
	    for node in self.nodes:
		if self.nodes[node] > threshold and node in self.graph.nodes():
		    labels[node] = node
		else:
		    pass
	elif type(threshold) == float:
	    pass #TODO: baseado em porcentagem, eg: threshold=0.01 -> 1% dos nos mais pesados tem labels.
	else:
	    pass
	return labels

    def save_analysis(self, folder_path = None):
	attributes = self.__dict__.keys()

	for attr in attributes:
	    filename = folder_path + '/' + attr + '.pickle'
	    attr_file = open(filename, 'wb')
	    attr_obj = getattr(self, attr)
	    try:
		pickle.dump(attr_obj.copy(), attr_file, protocol = pickle.HIGHEST_PROTOCOL)
		print('wrote {}... (with .copy())'.format(filename))
	    except:
		pickle.dump(attr_obj, attr_file, protocol = pickle.HIGHEST_PROTOCOL)
		print('wrote {}...'.format(filename))
	    attr_file.close()

    def load_analysis(self, folder_path = None):
	pass

class NLPAnalysis(object):
    def __init__(self):
	self.nodes = list()
	self.edges = list()
    pass

def create_folder(project_name):#NOTE: certamente cheio de bugs
    try:
	os.makedirs(project_name)
    except OSError as e:
	if e.errno == errno.EEXIST:
	    tries = 1
	    project_name = '{}_{}'.format(project_name, tries)
	    while os.path.exists(project_name):
		tries += 1
		project_name = project_name.split('_').pop(0)
		project_name = '{}_{}'.format(project_name, tries)
	    create_folder(project_name)
	if e.errno != errno.EEXIST:
	    raise
	    #exit('E: make_project_folder')
    return project_name

def short_label(node):
    result = node
    splitted_node = node.split(', ')
    if len(splitted_node) > 1:
	result = '{} ({})'.format(splitted_node[0], splitted_node[1])
    return result

@Debug('setup')
def parse_lines(doc_filepath, chunk_size = 2048, sep = '\n'):
    string_buffer = ''
    with open(doc_filepath, 'r') as file_handle:
	for chunk in iter(lambda: file_handle.read(chunk_size), ''):
	    string_buffer += chunk
	    splitted_buffer_list = string_buffer.split(sep)
	    while len(splitted_buffer_list) > 1:
		line = splitted_buffer_list.pop(0)
		if line:
		    yield line
		else:
		    pass

	    string_buffer = ''.join(splitted_buffer_list)
	else:
	    yield string_buffer

@Debug('setup')
def load_wos_data_ex(file_path, count = None, output = None):
    #TODO: cuidar de multiplos DOIs.
    result = list()
    doc_list = list()
    temp_field = str()
    temp_data = str()
    temp_doc = dict()
    for line in parse_lines(file_path):
	if line[:2] == 'ER':
	    pass
	    doc_list.append(temp_doc.copy())
	    temp_doc = {}
	    if count and count < len(doc_list):
		break
	elif line[:2] == 'EF':
	    pass
	elif line[:2] != '  ':
	    pass
	    temp_field = line[:2]
	    temp_doc[temp_field] = []
	    temp_data = line[3:]
	    temp_doc[temp_field].append(temp_data)
	elif line[:2] == '  ':
	    pass
	    temp_data = line[3:]
	    temp_doc[temp_field].append(temp_data)
	else:
	    pass

    result = format_wos_data(doc_list)

    return result

@Debug('setup')
def load_wos_data(file_path, count = None, output = None):
    result = list()
    doc_list = list()

    input_file = open(file_path, 'r')
    file_string = input_file.read()
    input_file.close()
    lines = file_string.split('\n')

    temp_field = str()
    temp_data = str()
    temp_doc = dict()

    for line in lines:
	if line[:2] == 'ER':
	    pass
	    doc_list.append(temp_doc.copy())
	    temp_doc = {}
	    if count and count < len(doc_list):
		break

	elif line[:2] == 'EF':
	    pass

	elif line[:2] != '  ':
	    pass
	    temp_field = line[:2]
	    temp_doc[temp_field] = []
	    temp_data = line[3:]
	    temp_doc[temp_field].append(temp_data)

	elif line[:2] == '  ':
	    pass
	    temp_data = line[3:]
	    temp_doc[temp_field].append(temp_data)

	else:
	    pass

    result = format_wos_data(doc_list)
    return result

def pandas_format(doc_dict):
    pass

def make_db(doc_dict, db_name = '/wos.db'):
    #TODO: consertar para caso em que o wos.db ja foi criado
    import sqlite3
    connector = sqlite3.connect(db_name)
    connector.text_factory = str
    c = connector.cursor()

    c.execute(u"""CREATE TABLE main_table (
    id integer,
    au text_factory,
    ti title_factory,
    di text,
    py integer);""")
    c.execute(u"""CREATE TABLE reference_table (
    id integer,
    cd text_factory);""")
    c.execute(u"""CREATE TABLE keyword_table (
    id integer,
    kw text_factory);""")

    doc_list = doc_dict.values()
    for doc_index, doc in enumerate(doc_list):

	c.execute("""INSERT INTO main_table (id, au, ti, di, py) VALUES (
	?,
	?,
	?,
	?,
	?);""", (doc_index, doc['authors'][0], doc['title'], doc['doi'],
	         doc['publication_year']))

	for ref_index in range(1, len(doc['references'])):
	    c.execute("""INSERT INTO reference_table (id, cd) VALUES  (
	    ?,
	    ?);""", (doc_index, doc['references'][ref_index]))

	for kw_index in range(1, len(doc['keywords'])):
	    c.execute("""INSERT INTO keyword_table (id, kw) VALUES (
	    ?,
	    ?);""", (doc_index, doc['keywords'][kw_index]))

    connector.commit()
    connector.close()

@Debug('nw')
def make_network(doc_dict, node_weight = None, edge_weight = None, ):
    nw = Network()

    for doc_index, doc in doc_dict.items():
	ref_list = doc['references']
	for i, ref_i in enumerate(ref_list):
	    nw.nodes[ref_i] += 1
	    for j, ref_j in enumerate(ref_list,start=i):
		nw.nodes[ref_j] += 1
		ref_left  = min(ref_i, ref_j)
		ref_right = max(ref_i, ref_j)
		nw.edges[(ref_left, ref_right)] += 1
	
    for node in nw.nodes:
	doi = doi_from_node(node)
	if doi:
	    nw.doi_table[doi] = node
	
    return nw

@Debug('nw')
def network_analysis(nw, node_weight = None, edge_weight = None, 
	             giant_component_only = True, use_threading = True):
    nw.graph = nx.Graph()

    nw.graph.add_nodes_from([node for node in nw.nodes 
			    if nw.nodes[node] > node_weight])
    nw.graph.add_edges_from([(edge[0],edge[1]) for edge in nw.edges 
			    if nw.edges[edge] > edge_weight])

    if giant_component_only: #giant component
	nw.giant_component(inplace = True)

    if use_threading: #threading
	nw.analysis()
	
    else:
	nw.eval_betweenness_centrality()
	nw.pos = nw.eval_pos()
	nw.eval_partitions()
	print nw.nodes_pos

    nw.nodes_color = [partition_colors[nw.partitions[n]%len(partition_colors)] 
			for n in nw.graph.nodes]

@Debug('nw')
def draw_network(nw, draw_labels = False, threaded = True):
    fig = plt.figure('Network', figsize=(8,5))
    ax = fig.gca()
    plt.xticks(list())
    plt.yticks(list())

    if threaded:
	thread_list = list()
	t1 = threading.Thread(target = nx.draw_networkx_nodes, args = (nw.graph,                              #G
								       nw.nodes_pos,                          #pos
								       None,                                  #nodelist
								       [nw.nodes[node] for node in nw.nodes], #node_size
								       nw.nodes_color,                        #node_color
								       'o',                                   #node_shape
								       1.0))                                  #alpha
	thread_list.append(t1)

	t2 = threading.Thread(target = nx.draw_networkx_edges, args = (nw.graph,     #G
								       nw.nodes_pos, #pos
								       None,         #edgelist
								       1.0,          #width
								       '#A0A0A0',    #edge_color
								       'solid',      #style
								       1.0))         #alpha
	thread_list.append(t2)
	    

	if draw_labels: #TODO: utilizar event handling fo matplotlib para mostrar label interativamente.
	    t3 = threading.Thread(target = nx.draw_networkx_labels, args = (nw.graph,          #G
					                                    nw.nodes_pos,      #pos
									    nw.make_labels(3), #label
									    9,                 #font_size
									    'k',               #font_color
									    'sans-serif',      #font_family
									    'normal',          #font_weight
									    0.8))              #alpha
	    thread_list.append(t3)

	for thread in thread_list:
	    thread.start()

	for thread in thread_list:
	    thread.join()

    else:
	nx.draw_networkx_nodes(G = nw.graph,
			       pos = nw.nodes_pos,
			       node_size = [nw.nodes[node] for node in nw.nodes],
			       node_color = nw.nodes_color)

	nx.draw_networkx_edges(G = nw.graph,
			       pos = nw.nodes_pos,
			       edge_color = '#A0A0A0')

	if draw_labels:
	    nx.draw_networkx_labels(G = nw.graph,
				    pos = nw.nodes_pos,
				    labels = nw.make_labels(3))
    plt.show()

@Debug('setup')
def format_wos_data(wos_list):
    result = {}

    for doc_index, doc in enumerate(wos_list):
	result[doc_index] = {}
	if 'AB' in doc:
	    result[doc_index]['abstract'] = ' '.join(doc['AB'])
	else:
	    result[doc_index]['abstract'] = ''
	if 'TI' in doc:
	    result[doc_index]['title'] = ' '.join(doc['TI'])
	else:
	    result[doc_index]['title'] = ''
	if 'PY' in doc:
	    result[doc_index]['publication_year'] = ' '.join(doc['PY'])
	else:
	    result[doc_index]['publication_year'] = ''
	if 'AU' in doc:
	    result[doc_index]['authors'] = doc['AU']
	else:
	    result[doc_index]['authors'] = []
	if 'CR' in doc:
	    result[doc_index]['references'] = doc['CR']
	else:
	    result[doc_index]['references'] = []
	if 'ID' in doc:
	    result[doc_index]['keywords'] = (' '.join(doc['ID'])).split(';')
	else:
	    result[doc_index]['keywords'] = []
	if 'DI' in doc:
	    result[doc_index]['doi'] = doc['DI'][0]
	else:
	    result[doc_index]['doi'] = ''
    return result

@Debug('setup')
def make_doi_table(doc_dict):
    result = dict()
    for doc in doc_dict:
	result[doc_dict[doc]['doi']] = doc
    return result

def doi_from_node(node): 
    result = node.split('DOI ')
    if len(result) > 1:
	return result[1]
    else:
	return ''

def nltk_process_abstract(doc):
    abstract_tokens = nltk.word_tokenize(doc['abstract'])
    abstract_text = nltk.Text(abstract_tokens)
    abstract_tags = nltk.pos_tag(abstract_text)

    tags_fd = nltk.FreqDist(tag for (word, tag) in abstract_tags)
    return tags_fd

def abstract_nlp(clusters, exclusion_list = global_word_exclusion_list):
    nltk_analysis_result = dict()
    for p in clusters:
	p_abstract = ''
	for node in clusters[p]:
	    try:
		node_doi = doi_from_node(node)
		if node_doi:
		    p_abstract += doc_dict[doi_table[node_doi]]['abstract']
		    p_abstract += '\n'
		else:
		    pass
	    except:
		print 'EXCEPT(abstract_nlp) -- node:', node
		pass

	abstract_tokens = nltk.word_tokenize(p_abstract)
	abstract_text = nltk.Text(abstract_tokens)
	abstract_fd = nltk.FreqDist(abstract_text)
	nltk_analysis_result[p] = list()

	for f in abstract_fd:
	    if not f in exclusion_list:
		nltk_analysis_result[p].append(f)

    return nltk_analysis_result

def spacy_ngram_model(doc_dict, text = 'title', n = 0):
    #TODO: n-gram model as dict -> result[0] unigram
    #				-> result[1] bigram
    #				-> ...
    #				-> result[n-1] n-gram
    # criterios: n == n_max OU entropy(n) - entropy(n-1) <= delta_entropy_threshold
    string_corpus = u'.\n'.join([doc_dict[doc][text] for doc in doc_dict])
    parsed_corpus = nlp(string_corpus)

    sentences = [[unicode(token.string.strip()) for token in sent
		    if not token.is_punct and
		    not token.is_space and
		    not token.is_stop]
		     for sent in parsed_corpus.sents]

    phrases = Phrases(sentences)

    bigram_model = Phraser(phrases)
    trigram_model = Phrases(bigram_model[sentences])
    return bigram_model, trigram_model

def cluster_analysis(network, doc_dict, doi_table, ngram_model, narrative = None, build_folder = None):
    clusters = network['clusters']
    bigram_model = ngram_model[0]
    trigram_model = ngram_model[1]
    nlp_clusters = defaultdict(list)

    for c in clusters:
	for n in clusters[c]:
	    doi = doi_from_node(n)
	    if not doi: continue
	    if not doi in doi_table: continue

	    doc_key = doi_table[doi]
	    nlp_clusters[c].append(doc_key)

    token_freq = dict()

    for c in nlp_clusters:
	token_freq[c] = defaultdict(int)
	for doc in nlp_clusters[c]:

	    doc_abstract = doc_dict[doc]['abstract']

	    parsed_doc_abstract = nlp(unicode(doc_abstract))

	    abstract_unigrams = [token.lemma_ for token in parsed_doc_abstract
						if not token.is_punct and
						not token.is_stop]

	    abstract_bigrams = bigram_model[abstract_unigrams]
	    abstract_trigrams = trigram_model[abstract_bigrams]

	    for abstract_token in abstract_trigrams:
		token_freq[c][abstract_token] += 1

	plt.figure('cluster {}'.format(c))
	plot_token_histogram(token_freq[c])
	if build_folder:
	    plt.savefig('{}/cluster{}.png'.format(build_folder,c))

    for c in token_freq:
	print('\ncluster {}:\ntokens, freq:'.format(c))
	for token in token_freq[c]:
	    if token_freq[c][token] > 1:
		pass

    narrative['nlp_clusters'] = nlp_clusters.copy()
    narrative['token_freq'] = token_freq.copy()

def save_obj(obj, filename):
    file_handle = open(filename, 'w')
    pickle.dump(obj, file_handle)
    file_handle.close()

def make_narrative(narrative, doc_dict, network, build_folder = None):
    #TODO: templates html para gerar pagina com resumo dos resultados, e.g.:
    #print 'func(make_narrative):', narrative
    narr_path = '/narrative.html'
    result = '<html>'

    html_title = lambda t : '<title>{}</title>'.format(t)
    html_head = lambda h : '<head>{}</head>'.format(h)
    html_hn = lambda h, n : '<h{0}>{1}</h{0}>'.format(n, h)
    html_pn = lambda p, n : '<p{0}>{1}</p{0}>'.format(n, p)
    html_body = lambda b : '<body>{}</body>'.format(b)
    html_figure = lambda src : '<figure><img src=\"{}\"></figure>'.format(src)
    html_bold = lambda t : '<b>{}</b>'.format(t)
    html_br = '<br>'

    result += html_title(narr_path.split('.')[0])

    if build_folder:
	result += html_figure(build_folder+'test.png')

    if 'nlp_clusters' in narrative:
	for c in narrative['nlp_clusters']:
	    result += html_hn('cluster {}'.format(c),1)
	    for doc_key in narrative['nlp_clusters'][c]:
		result += html_pn(html_bold('title: ') + doc_dict[doc_key]['title'],1)
		result += html_br
		result += html_pn(html_bold('authors: ')+ '; '.join(doc_dict[doc_key]['authors']),1)
		result += html_br
		result += html_pn(html_bold('abstract: ')+ doc_dict[doc_key]['abstract'],1)
		result += html_br
		result += html_br
	    
    indices = range(len(narrative['token_freq'].keys()))
    doc_df = pd.DataFrame(narrative['token_freq'].values(), index=indices)

    df = doc_df.T
    df = df.fillna(0.0)

    if build_folder:
	save_obj(doc_df, build_folder+'/token_freq_df.pickle')

    for c in df.columns:
	result += html_hn('cluster {}'.format(c+1),1)
	result += pd.DataFrame(df.sort_values(c, ascending=False)[c][:20]).to_html().encode('utf-8')
	result += html_br

    doc_html = doc_df.to_html().encode('utf-8')

    result += html_head('Raw data:')
    result += html_br
    result += html_br
    result += doc_html
    result += '</html>'

    narr_file_handle = open(build_folder + narr_path, 'w')
    narr_file_handle.write(result)
    narr_file_handle.close()

    return result

def gen_corpus_string(doc_dict, field='abstract'):
    for doc in doc_dict:
	yield unicode(doc_dict[doc][field])

@Debug('nlp')
def gen_lemmatized_sents(doc_dict, exclusion_list = global_word_exclusion_list):
    for parsed_corpus in nlp.pipe(gen_corpus_string(doc_dict),
				  batch_size = 100,
				  n_threads = 4):
	for sent in parsed_corpus.sents:
	    yield [token.lemma_ for token in sent
		    if token.lemma_ not in exclusion_list]

def make_trigram_model(sentences_list):
    bigram_list = Phrases(sentences_list)
    bigram_model = Phraser(bigram_list)
    bigram_sentences = bigram_model[sentences_list]

    trigram_list = Phrases(bigram_sentences)
    trigram_model = Phraser(trigram_list)
    return bigram_model, trigram_model

def make_ngram_model(sentences, n = 2): #TODO: concertar... deepcopy???
    ngram_models = list()

    igram_sentences = sentences
    for i in range(n):
	igram_list = Phrases(igram_sentences)
	igram_model = Phraser(igram_list)
	ngram_models.append(igram_model)
	for model in ngram_models:
	    igram_sentences = model[igram_sentences]

	print('{}-gram model completed'.format(i+2))

def main():
####NOTE: config #########################################
    data_folder = './data'
    project_folder = './projects'
    data_filename = '/sample_wos_data.txt'
    project_name = 'untitled'

    nw_doc_count = 270
    nw_node_weight = 6
    nw_edge_weight = 5

##########################################################

    output_folder = create_folder(project_folder + '/' + project_name)

    doc_dict = load_wos_data_ex(data_folder + data_filename, count=nw_doc_count)

    nw = make_network(doc_dict)

    network_analysis(nw, node_weight = nw_node_weight, edge_weight = nw_edge_weight, 
		     giant_component_only = True, use_threading = True)

    nw.make_df(doc_dict)
    nw.eval_clusters()

    nw.save_analysis(output_folder)
    draw_network(nw)
    plt.show()

    sentences_list = [sent for sent in gen_lemmatized_sents(doc_dict)]
    #ngram_models = make_ngram_model(sentences_list)
    bigram_model, trigram_model = make_trigram_model(sentences_list)
	
main()

print '\n------\nclocks:'
for clock in Debug.func_clock:
    print clock, ':', (Debug.func_clock[clock][1] - Debug.func_clock[clock][0])
print '-----\n'
